import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        int number; 
        int Sum = 0;  
        int count = 0;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Please input number: ");
            number = sc.nextInt();
            Sum = Sum + number;
            count++;
            System.out.println("Sum: "+ Sum + " Avg: " + (((double)Sum)/count));
        }
        while (number != 0);
        System.out.println("Bye");
        sc.close();
    }
}
